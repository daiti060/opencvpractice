import cv2
import dlib
import numpy as np
# 顔検出器の読み込み
detector = dlib.get_frontal_face_detector()
# 顔認識モデルの読み込み
net = cv2.dnn.readNetFromCaffe(r'C:\Users\daiti\source\OpenCVTest\deploy.prototxt', r'C:\Users\daiti\source\OpenCVTest\res10_300x300_ssd_iter_140000_fp16.caffemodel')
# 名前と画像ファイルの対応リスト
names = {
    'FujiwaraDaichi': r'C:\Users\daiti\source\OpenCVTest\img\FujiwaraDaichi.jpg',
    'KuwanoYumena': r'C:\Users\daiti\source\OpenCVTest\img\KuwanoYumena.jpg',
    'person2': r'C:\Users\daiti\source\OpenCVTest\img\dummy.jpg',
    'person3': r'C:\Users\daiti\source\OpenCVTest\img\dummy1.png'
}

# 画像ファイルを顔画像として読み込む
face_images = {name: cv2.imread(path) for name, path in names.items()}

# カメラキャプチャの開始
cap = cv2.VideoCapture(0)

while True:
    # フレームの読み込み
    ret, img = cap.read()

    # 顔検出
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = detector(gray)

    # 検出された顔の領域に名前を表示する
    for face in faces:
        # 顔領域に四角形を描画
        x, y, w, h = face.left(), face.top(), face.width(), face.height()
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)

        # 顔画像との一致を確認
        face_roi = img[y:y+h, x:x+w]
        # 顔認識の処理...
        face_roi = img[y:y+h, x:x+w]
        blob = cv2.dnn.blobFromImage(face_roi, 1.0, (300, 300), (104.0, 177.0, 123.0))
        net.setInput(blob)
        detections = net.forward()

        # 顔認識結果の取得
        confidence = detections[0, 0, 0, 2]
        if confidence > 0.5:
            # 最も類似度が高い名前を取得
            max_similarity = 0.0
            max_name = 'Unknown'
            for name, face_image in face_images.items():
                similarity = cv2.compareHist(cv2.calcHist([face_roi], [0], None, [256], [0, 256]),
                                             cv2.calcHist([face_image], [0], None, [256], [0, 256]),
                                             cv2.HISTCMP_CORREL)
                if similarity > max_similarity:
                    max_similarity = similarity
                    max_name = name

            # 顔領域に名前を表示
            cv2.putText(img, max_name, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (255, 0, 0), 2)
        else:
            # 顔領域に四角形を描画
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    # 画像の表示
    cv2.imshow('video image', img)

    # キー入力の待機
    key = cv2.waitKey(10)

    # ESCキーでループを終了
    if key == 27:
        break

# カメラキャプチャの停止
cap.release()

# ウィンドウの破棄
cv2.destroyAllWindows()
